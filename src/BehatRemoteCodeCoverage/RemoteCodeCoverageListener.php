<?php
declare(strict_types=1);

namespace BehatRemoteCodeCoverage;

use Behat\Behat\EventDispatcher\Event\ScenarioLikeTested;
use Behat\Behat\EventDispatcher\Event\ScenarioTested;
use Behat\Mink\Mink;
use Behat\Testwork\EventDispatcher\Event\AfterSuiteTested;
use Behat\Testwork\EventDispatcher\Event\BeforeSuiteTested;
use Behat\Testwork\EventDispatcher\Event\SuiteTested;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class RemoteCodeCoverageListener implements EventSubscriberInterface
{
    private ?string $coverageGroup = null;

    public function __construct(
        private readonly Mink $mink,
        private readonly string $baseUrl,
        private readonly bool $coverageEnabled,
    ) {
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SuiteTested::BEFORE => 'beforeSuite',
            ScenarioTested::BEFORE => 'beforeScenario',
            SuiteTested::AFTER => 'afterSuite'
        ];
    }

    public function beforeSuite(BeforeSuiteTested $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        $this->coverageGroup = uniqid($event->getSuite()->getName(), true);
    }

    public function beforeScenario(ScenarioLikeTested $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        $minkSession = $this->mink->getSession();
        if (!$minkSession->isStarted()) {
            $minkSession->start();
        }

        // We MUST visit the site before setting up the cookies
        $this->mink->getSession()->visit($this->baseUrl);

        $minkSession->setCookie('collect_code_coverage', true);
        $minkSession->setCookie('coverage_group', $this->coverageGroup);
        $minkSession->setCookie('coverage_id', $event->getFeature()->getFile() . ':' . $event->getNode()->getLine());
    }

    public function afterSuite(AfterSuiteTested $event): void
    {
        if (!$this->coverageEnabled) {
            return;
        }

        $this->reset();
    }

    private function reset(): void
    {
        $this->coverageGroup = null;
    }
}
