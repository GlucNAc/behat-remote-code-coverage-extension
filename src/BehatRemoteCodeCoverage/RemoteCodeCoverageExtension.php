<?php
declare(strict_types=1);

namespace BehatRemoteCodeCoverage;

use Behat\Testwork\ServiceContainer\Extension;
use Behat\Testwork\ServiceContainer\ExtensionManager;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

final class RemoteCodeCoverageExtension implements Extension
{
    public function process(ContainerBuilder $container): void
    {
    }

    public function getConfigKey(): string
    {
        return 'remote_code_coverage';
    }

    public function initialize(ExtensionManager $extensionManager): void
    {
    }

    public function configure(ArrayNodeDefinition $builder): void
    {
        $builder
            ->children()
                ->scalarNode('base_url')
                    ->defaultNull()
                    ->info('The base url of the php application, leave null to use mink base url.')
                ->end()
            ->end()
        ;
    }

    public function load(ContainerBuilder $container, array $config): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('remote_code_coverage.base_url', $config['base_url'] ?: '%mink.base_url%');
        $container->setParameter('remote_code_coverage.remote_coverage_enabled', 'true' === \getenv('BEHAT_COVERAGE'));
    }
}
